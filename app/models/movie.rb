require 'elasticsearch/model'
class Movie < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  has_many :roles
  has_many :crews, through: :roles

  mapping do
    indexes :id, index: :not_analyzed
    indexes :name
    indexes :synopsis
    indexes :year
    indexes :language
    indexes :country
    indexes :runtime,  type: 'integer'
    indexes :review,   type: 'float'
  end

  def as_indexed_json(options = {})
    self.as_json(only: [:id, :name, :synopsis, :year, :country, :language, :runtime, :review],
      include: {
      crews:  { only: [:id, :name] },
      roles: { only: [:id, :job] }
    })
  end
end

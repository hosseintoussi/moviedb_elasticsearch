json.extract! @movie, :id, :name, :synopsis, :country, :year, :review, :rating, :runtime, :language, :release_date, :created_at, :updated_at

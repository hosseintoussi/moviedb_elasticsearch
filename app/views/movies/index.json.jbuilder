json.array!(@movies) do |movie|
  json.extract! movie, :id, :name, :synopsis, :country, :year, :review, :rating, :runtime, :language, :release_date
  json.url movie_url(movie, format: :json)
end

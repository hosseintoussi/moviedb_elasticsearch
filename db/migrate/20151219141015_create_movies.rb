class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :name
      t.text :synopsis
      t.string :country
      t.integer :year
      t.float :review
      t.float :rating
      t.integer :runtime
      t.string :language
      t.date :release_date

      t.timestamps null: false
    end
  end
end

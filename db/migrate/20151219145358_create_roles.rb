class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.integer :movie_id
      t.integer :crew_id
      t.string :job

      t.timestamps null: false
    end
  end
end

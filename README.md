# README #
This is a movie DB app that uses elasticsearch. 

- A sample DB dump is in the source code, so you can restore it to get some data.

### Tips to get started ###
- Make sure you have elasticsearch installed.
- Setup the rails app. 
- Restore the DB dump.
- in rails console run the commands below to index the DB into elasticsearch.

```
#!ruby
Movie.__elasticsearch__.create_index! force: true
# and then:
Movie.import
# this will take a while.
```
- You are all set. Start rails app and navigate to: http://localhost:3000/movies